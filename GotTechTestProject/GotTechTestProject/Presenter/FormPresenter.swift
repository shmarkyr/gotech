//
//  FormPresenter.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 21.05.2023.
//

import Foundation

protocol FormPresenterProtocol {
    var items: [TableCellItemAnyType] { get set }

    func viewDidLoad()
    func didTap()
}

class FormPresenter {
    var items = [TableCellItemAnyType]()

    var answers: [String: String] = [:]
    var questionAmount = 0

    let view: FormViewProtocol
    let networkService: NetworkServiceProtocol

      // MARK: - Initiliaze

    init(
        view: FormViewProtocol,
        networkService: NetworkServiceProtocol
    ) {
        self.view = view
        self.networkService = networkService
    }

}

  // MARK: - FormPresenterProtocol
extension FormPresenter: FormPresenterProtocol {
    func viewDidLoad() {
        networkService.getQuestions { [weak self] result in
            switch result {
            case .success(let questions):
                self?.questionAmount = questions.count
                for question in questions {
                    if question.type == .text {
                        let item = TextFieldViewModel(questionName: question.title, hint: "Write your answer") { [weak self] text in
                            guard let self = self else { return }
                            self.saveAnswer(with: question.title, answer: text)
                        }
                        self?.items.append(item)
                    } else if question.type == .options {
                        let item = SurveyViewModel(title: question.title, options: question.choises) { answer in
                            self?.view.endEditing()
                            self?.saveAnswer(with: question.title, answer: answer)
                        }
                        self?.items.append(item)
                    }
                }
                DispatchQueue.main.async {
                    self?.view.updateView()
                }
            case .failure(_):
                DispatchQueue.main.async {
                    self?.view.showErrorAlert(with: "Network error", message: "Try again pls")
                }
            }
        }
    }

    func didTap() {
        networkService.saveAnswers(with: answers) { [weak self] result in
            switch result {
            case .success():
                break
            case .failure(_):
                DispatchQueue.main.async {
                    self?.view.showErrorAlert(with: "Something go Wrong!", message: "Can't save your answers...")
                }
            }
        }
    }
}

extension FormPresenter {
    private func validate() {
        if answers.count == questionAmount {
            view.setButtonEnabled(isEnabled: true)
        } else {
            view.setButtonEnabled(isEnabled: false)
        }
    }

    private func saveAnswer(with name: String, answer: String) {
        if answer == "" {
            answers.removeValue(forKey: name)
            view.setButtonEnabled(isEnabled: false)
        } else {
            answers[name] = answer
            validate()
        }
    }
}

