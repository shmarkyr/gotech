//
//  QuestionModel.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 21.05.2023.
//

import Foundation

/// Question Model
struct QuestionModel: Codable {
    /// Question ID
    var id: String
    /// Question Name
    var title: String
    /// Question Type
    var type: QType
    /// Question  Varints
    var choises: [String]?
}

/// Type of the questions 
enum QType: String, Codable {
    case text
    case options
}



