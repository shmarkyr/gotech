//
//  FormBuilder.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 22.05.2023.
//

import UIKit

enum FormBuilder {
    static func build(with networkService: NetworkServiceProtocol) -> UIViewController {
        let view = FormView()
        let presenter = FormPresenter(
            view: view,
            networkService: networkService
        )
        view.presenter = presenter
        return view
    }
}
