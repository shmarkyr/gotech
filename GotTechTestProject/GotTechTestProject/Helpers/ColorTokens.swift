//
//  ColorTokens.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 24.05.2023.
//

import UIKit

public struct ColorTokens {
    public static let dsTextAccent = UIColor(hex: "#8F49EC")
    public static let dsBackgroundBasic = UIColor(hex: "#FFFFFF")
    public static let dsTextSecondary = UIColor(hex: "#8C92A3")
    public static let dsTextPrimary = UIColor(hex: "#000614")
    public static let dsDividersDefault = UIColor(hex: "#E8EAF1")
    public static let dsBackgroundDisabled = UIColor(hex: "#CCD2DC")
}
