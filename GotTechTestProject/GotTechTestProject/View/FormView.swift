//
//  FormView.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 21.05.2023.
//

import UIKit

protocol FormViewProtocol {
    /// update Table View
    func updateView()
    /// End view responder
    func endEditing()
    /// Show allert
    func showErrorAlert(with text: String, message: String)
    func setButtonEnabled(isEnabled: Bool)
}

class FormView: UIViewController {

    var presenter: FormPresenterProtocol?

    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 120
        tableView.rowHeight = UITableView.automaticDimension
        tableView.separatorStyle = .none
        tableView.backgroundColor = .clear
        tableView.translatesAutoresizingMaskIntoConstraints = false
        tableView.register(TextFieldView.self, forCellReuseIdentifier: TextFieldView.reuseID)
        tableView.register(SurveyView.self, forCellReuseIdentifier: SurveyView.reuseID)
        tableView.allowsSelection = false
        return tableView
    }()

    lazy var submitButton: UIButton = {
        let submitButton = UIButton()
        submitButton.translatesAutoresizingMaskIntoConstraints = false
        submitButton.layer.masksToBounds = true
        submitButton.layer.cornerRadius = 12
        submitButton.setBackgroundColor(color: ColorTokens.dsTextAccent, for: .normal)
        submitButton.setBackgroundColor(color: ColorTokens.dsBackgroundDisabled, for: .disabled)
        submitButton.setTitle("Submit", for: .normal)
        submitButton.titleLabel?.font = .boldSystemFont(ofSize: 22)
        submitButton.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
        submitButton.isEnabled = false
        return submitButton
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setUp()
        presenter?.viewDidLoad()
    }

    func setUp() {
        addSubviews()
        setUpLayout()
        setUpAppearance()
    }

    func addSubviews() {
        [tableView, submitButton].forEach {
            view.addSubview($0)
        }
    }

    func setUpLayout() {
        NSLayoutConstraint.activate([
            /// TableView constraints
            tableView.topAnchor.constraint(equalTo: view.topAnchor),
            tableView.leftAnchor.constraint(equalTo: view.leftAnchor),
            tableView.rightAnchor.constraint(equalTo: view.rightAnchor),
            tableView.bottomAnchor.constraint(equalTo: submitButton.topAnchor, constant: -10),

            /// SubmitButton constraints
            submitButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 32),
            submitButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -32),
            submitButton.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: -32),
            submitButton.heightAnchor.constraint(equalToConstant: 52),
        ])
    }

    func setUpAppearance() {
        view.backgroundColor = ColorTokens.dsBackgroundBasic
        title = "Gotech Quiz"
    }


    @objc
    func didTapButton() {
        presenter?.didTap()
    }
}

  // MARK: - FormViewProtocol
extension FormView: FormViewProtocol {
    func updateView() {
        tableView.reloadData()
    }

    func showErrorAlert(with text: String, message: String) {
        let alert = UIAlertController(title: text, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        present(alert, animated: true)
    }

    func endEditing() {
        view.endEditing(true)
    }

    func setButtonEnabled(isEnabled: Bool) {
        submitButton.isEnabled = isEnabled
    }
}

  // MARK: - UITableViewDataSource, UITableViewDelegate
extension FormView: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return presenter?.items.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let model = presenter?.items[indexPath.row]
        return tableView.dequeCell(cellModel: model , indexPath: indexPath)
    }
}
