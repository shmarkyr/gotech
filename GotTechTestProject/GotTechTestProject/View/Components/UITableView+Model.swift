//
//  UITableView+Model.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 24.05.2023.
//

import UIKit


public protocol TableCellView: UITableViewCell {
    static var reuseID: String { get }

    func setup(model: TableCellItemAnyType)
}

public protocol TableCellItemAnyType {
    var cellType: TableCellView.Type { get }
}

public protocol TableCellItem: TableCellItemAnyType {
    associatedtype Cell: TableCellView
}

public extension TableCellItem  {
    var cellType: TableCellView.Type {
        return Cell.self
    }
}

public extension UITableView {
    func dequeCell(cellModel: TableCellItemAnyType?,
                   indexPath: IndexPath) -> UITableViewCell {
        guard let cellModel = cellModel,
              let cell = dequeueReusableCell(withIdentifier: cellModel.cellType.reuseID,
                                             for: indexPath) as? TableCellView else {
            return UITableViewCell()
        }
        cell.setup(model: cellModel)
        cell.contentView.isUserInteractionEnabled = false
        return cell
    }
}
