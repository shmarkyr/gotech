//
//  SurveyViewModel.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 24.05.2023.
//

import Foundation

protocol SurveyViewModelProtcol {
    var onTap: ((String) -> Void)? { get set }
}

final class SurveyViewModel: SurveyViewModelProtcol, TableCellItem {
      // MARK: - TableCellItem
    typealias Cell = SurveyView

    var title: String
    var options: [String]?
    var onTap: ((String) -> Void)?

    init(title: String, options: [String]?, onTap: ((String) -> Void)?) {
        self.title = title
        self.options = options
        self.onTap = onTap
    }
}
