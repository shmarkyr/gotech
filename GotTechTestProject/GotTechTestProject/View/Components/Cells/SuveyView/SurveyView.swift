//
//  SurveyView.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 24.05.2023.
//

import UIKit

class SurveyView: UITableViewCell, TableCellView {

      // MARK: - TableCellView
    static var reuseID: String = "SurveyView"

    var viewModel: SurveyViewModel?

    var optionButtons: [CheckBoxItem] = []

    var answerOptions: [String] = [] {
        didSet {
            createOptionButtons()
        }
    }

    private var selectedOptionIndex: Int?

    private var textFieldDelegate: UITextFieldDelegate?

      // MARK: - UI components
    let questionLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textColor = ColorTokens.dsTextAccent
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.numberOfLines = 0
        return label
    }()

    func setup(model: TableCellItemAnyType) {
        guard let model = model as? SurveyViewModel else { return }
        self.viewModel = model
        setupViews()
        configure(with: model)
    }

    private func configure(with model: SurveyViewModel) {
        questionLabel.text = model.title
        if let options = model.options {
            answerOptions = options
        }
    }
    private func setupViews() {
        addSubview(questionLabel)

        NSLayoutConstraint.activate([
            questionLabel.topAnchor.constraint(equalTo: topAnchor, constant: 8),
            questionLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            questionLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
        ])
    }

    private func createOptionButtons() {

        for (index, option) in answerOptions.enumerated() {
            let optionButton = CheckBoxItem()
            optionButton.translatesAutoresizingMaskIntoConstraints = false
            optionButton.title = option
            optionButton.titleColor = ColorTokens.dsTextAccent
            optionButton.checkedImage = UIImage(named: "circleFill")
            optionButton.uncheckedImage = UIImage(named: "circle")
            addSubview(optionButton)
            optionButton.addTarget(self, action: #selector(optionButtonTapped(_:)), for: .touchUpInside)
            optionButton.isChecked = index == selectedOptionIndex

            NSLayoutConstraint.activate([
                optionButton.topAnchor.constraint(equalTo: questionLabel.bottomAnchor, constant: CGFloat(index * 40) + 8),
                optionButton.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
                optionButton.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32)
            ])

            optionButtons.append(optionButton)
        }

        if let lastButton = optionButtons.last {
            lastButton.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -16).isActive = true
        }
    }

    @objc private func optionButtonTapped(_ sender: CheckBoxItem) {
        guard let selectedOptionIndex = optionButtons.firstIndex(of: sender),
              let title = sender.title else {
                  return
              }

        viewModel?.onTap?(title)
        self.selectedOptionIndex = selectedOptionIndex

        for (index, button) in optionButtons.enumerated() {
            button.isChecked = index == selectedOptionIndex
        }
    }

}
