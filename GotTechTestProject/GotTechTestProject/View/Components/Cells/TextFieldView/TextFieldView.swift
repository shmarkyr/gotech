//
//  TextFieldView.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 24.05.2023.
//

import Foundation
import UIKit


class TextFieldView: UITableViewCell, TableCellView {

    private var textFieldViewModel: TextFieldViewModelProtocol?

      // MARK: - TableCellView
    static var reuseID: String = "TextFieldViewID"

      // MARK: - UI Components
    let nameLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.boldSystemFont(ofSize: 18)
        label.textColor = ColorTokens.dsTextAccent
        return label
    }()

     let textField: UITextField = {
         let textField = UITextField()
         textField.translatesAutoresizingMaskIntoConstraints = false
         textField.textColor = ColorTokens.dsTextPrimary
         textField.borderStyle = .none
         return textField
     }()

     let divider: UIView = {
         let view = UIView()
         view.translatesAutoresizingMaskIntoConstraints = false
         view.backgroundColor = ColorTokens.dsTextAccent
         return view
     }()

      // MARK: - Initiliaze
     override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
         super.init(style: style, reuseIdentifier: reuseIdentifier)
         setupViews()
     }

     required init?(coder aDecoder: NSCoder) {
         super.init(coder: aDecoder)
     }

    func setup(model: TableCellItemAnyType) {
        guard let model = model as? TextFieldViewModel else { return }
        self.textFieldViewModel = model
        nameLabel.text = model.questionName
        textField.placeholder = model.hint
        textField.delegate = self
    }

    private func setupViews() {
        addSubview(nameLabel)
        addSubview(textField)
        addSubview(divider)

        NSLayoutConstraint.activate([
            nameLabel.topAnchor.constraint(equalTo: topAnchor),
            nameLabel.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            nameLabel.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32),

            textField.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            textField.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            textField.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32),

            divider.topAnchor.constraint(equalTo: textField.bottomAnchor, constant: 8),
            divider.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 32),
            divider.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -32),
            divider.heightAnchor.constraint(equalToConstant: 1),

            bottomAnchor.constraint(equalTo: divider.bottomAnchor, constant: 16)
        ])
    }
}

  // MARK: - UITextFieldDelegate
extension TextFieldView: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        guard let text = textField.text else { return }
        textFieldViewModel?.onDidEndEditing?(text)
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}


