//
//  TextFieldViewModel.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 24.05.2023.
//

import Foundation
import UIKit

protocol TextFieldViewModelProtocol {
    var questionName: String { get set }
    var text: String? { get set }
    var hint: String? { get set }
    var onDidEndEditing: ((String) -> Void)? { get set }
}

class TextFieldViewModel: TextFieldViewModelProtocol, TableCellItem {
      // MARK: - TableCellItem
    typealias Cell = TextFieldView

    var questionName: String
    var text: String?
    var hint: String?
    var onDidEndEditing: ((String) -> Void)?

    init(
        questionName: String,
        text: String? = nil,
        hint: String? = nil,
        onDidEndEditing: ((String) -> Void)? = nil
    ) {
        self.questionName = questionName
        self.text = text
        self.hint = hint
        self.onDidEndEditing = onDidEndEditing
    }
}
