//
//  CheckBoxItem.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 25.05.2023.
//

import UIKit

class CheckBoxItem: UIControl {
    private let checkBoxImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        return imageView
    }()

    private let titleLabel: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16)
        return label
    }()

    var isChecked: Bool = false {
        didSet {
            updateAppereance()
            sendActions(for: .valueChanged)
        }
    }

    var title: String? {
        didSet {
            titleLabel.text = title
        }
    }

    var titleColor: UIColor? {
        didSet {
            updateAppereance()
        }
    }

    var checkedImage: UIImage? {
        didSet {
            updateAppereance()
        }
    }

    var uncheckedImage: UIImage? {
        didSet {
            updateAppereance()
        }
    }

    override var isEnabled: Bool {
        didSet {
            updateAppereance()
            titleLabel.isEnabled = isEnabled
        }
    }

    override var canBecomeFirstResponder: Bool {
        return true
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }

    private func setupViews() {
        addSubview(checkBoxImageView)
        addSubview(titleLabel)

        NSLayoutConstraint.activate([
            checkBoxImageView.leadingAnchor.constraint(equalTo: leadingAnchor),
            checkBoxImageView.centerYAnchor.constraint(equalTo: centerYAnchor),
            checkBoxImageView.widthAnchor.constraint(equalToConstant: 24),
            checkBoxImageView.heightAnchor.constraint(equalToConstant: 24),

            titleLabel.leadingAnchor.constraint(equalTo: checkBoxImageView.trailingAnchor, constant: 12),
            titleLabel.trailingAnchor.constraint(equalTo: trailingAnchor),
            titleLabel.topAnchor.constraint(equalTo: topAnchor),
            titleLabel.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    @objc private func checkboxControlValueChanged() {
        isChecked.toggle()
        sendActions(for: .valueChanged)
    }

    private func updateAppereance() {
        if isChecked {
            checkBoxImageView.image = checkedImage
        } else {
            checkBoxImageView.image = uncheckedImage
        }

        tintColor = titleColor
        checkBoxImageView.tintColor = ColorTokens.dsTextAccent
    }
}
