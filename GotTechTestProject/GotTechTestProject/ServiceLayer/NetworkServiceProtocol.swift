//
//  NetworkServiceProtocol.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 22.05.2023.
//

import Foundation

typealias QuestionResponce = Result<[QuestionModel], NetworkError>

protocol NetworkServiceProtocol {
    /// get all questions
    func getQuestions(completion: @escaping (QuestionResponce) -> Void)
    /// save answers
    func saveAnswers(
        with answers: [String: String],
        completion: @escaping(Result<Void, NetworkError>) -> Void
    )
}
