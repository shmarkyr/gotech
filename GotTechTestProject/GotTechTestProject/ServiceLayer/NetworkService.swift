//
//  NetworkService.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 22.05.2023.
//

import Foundation

enum NetworkConstant {
    static let questionURL = "http://localhost:3000/questions"
    static let answerURL = "http://localhost:3000/answers"
}

class NetworkService: NetworkServiceProtocol {
    // MARK: - Properties
    private let session: URLSession = .shared
    private let decoder = JSONDecoder()

    func getQuestions(completion: @escaping(QuestionResponce) -> Void) {
        guard  let questionUrl = URL(string: NetworkConstant.questionURL) else { return }
        var request = URLRequest(url: questionUrl)
        request.httpMethod = "GET"
        let dataTask = session.dataTask(with: request) { data, responce, error in
            do {
                let data = try self.httpResponse(data: data, response: responce)
                let responce = try self.decoder.decode([QuestionModel].self, from: data)
                completion(.success(responce))
            } catch let error as NetworkError {
                completion(.failure(error))
            } catch {
                completion(.failure(.unknown))
            }
        }
        dataTask.resume()
    }

    func saveAnswers(with answers: [String: String], completion: @escaping(Result<Void, NetworkError>) -> Void) {
        guard  let answersUrl = URL(string: NetworkConstant.answerURL) else { return }
        var request = URLRequest(url: answersUrl)
        request.httpMethod = "POST"
        guard let bodyData = answers.percentEncoded() else { return }
        request.httpBody = bodyData
        let dataTask = session.dataTask(with: request) { data, responce, error in
            if data != nil {
                completion(.success(()))
            } else if let error = error as? NetworkError {
                completion(.failure(error))
            } else {
                completion(.failure(.unknown))
            }
        }
        dataTask.resume()
    }



    // MARK: - httpResponse
    private func httpResponse(data: Data?, response: URLResponse?) throws -> Data {
        guard let httpResponse = response as? HTTPURLResponse,
              (200..<300).contains(httpResponse.statusCode),
              let data = data else {
            throw NetworkError.network
        }
        return data
    }
}
