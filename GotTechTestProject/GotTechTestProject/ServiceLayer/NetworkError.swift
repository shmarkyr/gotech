//
//  NetworkError.swift
//  GotTechTestProject
//
//  Created by Марк Шнейдерман on 22.05.2023.
//

import Foundation

// MARK: - NetworkError
enum NetworkError: Error {
    case network
    case unknown
}
